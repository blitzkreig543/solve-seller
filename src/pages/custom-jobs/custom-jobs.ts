import { Component, OnInit } from '@angular/core';

import { NavController, ModalController, Events, ToastController } from 'ionic-angular';
import { IJob } from '../../shared/interfaces';
import { DataService } from '../../shared/services/data.service';
import { AuthService } from '../../shared/services/auth.service';
import { ItemsService } from '../../shared/services/items.service';
import { MappingsService } from '../../shared/services/mappings.service';
import { JobComponent } from '../../shared/components/chefjob.component';
import { UserAvatarComponent } from '../../shared/components/user-avatar.component';

/*
  Generated class for the CustomJobs page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-custom-jobs',
  templateUrl: 'custom-jobs.html'
})
export class CustomJobsPage implements OnInit {
  public jobs: Array<IJob> = [];
  public category: string;
  public loading: boolean = true;


  constructor(public navCtrl: NavController,
    private modalCtrl: ModalController,
    private dataService: DataService,
    private authService: AuthService,
    private itemsService: ItemsService,
    private mappingsService: MappingsService,
    private toastCtrl: ToastController,
    private events: Events) {

  }

  ngOnInit() {
    var self = this;
    self.loadJobs(true);
  }

  loadJobs(fromStart: boolean) {
    var self = this;
    if(fromStart)
    self.loading = true;
    self.jobs = [];
    let uid = self.authService.getLoggedInUser().uid;
    this.dataService.getCategory(uid).then(function (snapshot) {
      self.category = snapshot.val();
      console.log('seller category' + self.category);
      self.dataService.getCategoryJobsRef().child('custom').orderByChild('jobStatus').equalTo('open').once('value', function (snapshot) {
        self.itemsService.reversedItems<IJob>(self.mappingsService.getJobs(snapshot)).forEach(function (job) {
          self.jobs.push(job);
          console.log(job);
          self.loading = false;
        });
      });
    });
  }
    reloadJobs(refresher) {
      this.loadJobs(true);
      refresher.complete();
  }
}
