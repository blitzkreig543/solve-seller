import { Injectable } from '@angular/core';
import { IJob, IQuotation, IMessage } from '../interfaces';
import { ItemsService } from '../services/items.service';

@Injectable()
export class MappingsService {

    constructor(private itemsService: ItemsService) { }

    getJobs(snapshot: any): Array<IJob> {
        let jobs: Array<IJob> = [];
        if (snapshot.val() == null)
            return jobs;

        let list = snapshot.val();

        Object.keys(snapshot.val()).map((key: any) => {
            let job: any = list[key];
            jobs.push({
                key: key,
                jobCategoryId: job.jobCategoryId,
                dateCreated: job.dateCreated,
                user: {
                    uid: job.user.uid,
                    username: job.user.username
                },
                quotations: job.quotations == null ? 0 : job.quotations,
                jobDate: job.jobDate,
                jobTime: job.jobTime,
                pincode: job.pincode,
                jobStatus: job.jobStatus,
                city: job.city,
                chefGuestsNumber: job.chefGuestsNumber,
                chefEventType: job.eventType,
                cleanNoRooms: job.cleanNoRooms,
                cleanAddRooms: job.cleanAddRooms,
                customJob: job.customJob,
                washNoClothes: job.washNoClothes,
                washService: job.washService,
                recycleType: job.recycleType,
                paintNoWalls: job.paintNoWalls,
                paintMaterials: job.paintMaterials,
                fAssemblyNoUnits: job.fAssemblyNoUnits,
                fMoveNoUnits: job.fMoveNoUnits,
                fMoveDestAddress: job.fMoveDestAddress
            });
        });
        return jobs;
    }

    getJob(snapshot: any, key: string): IJob {

        let job: IJob = {
            key: key,
            jobCategoryId: snapshot.jobCategoryId,
            dateCreated: snapshot.dateCreated,
            user: snapshot.user,
            quotations: snapshot.quotations == null ? 0 : snapshot.quotations,
            jobDate: snapshot.jobDate,
            jobTime: snapshot.jobTime,
            jobStatus: snapshot.jobStatus,
            pincode: snapshot.pincode,
            city: snapshot.city,
            chefGuestsNumber: snapshot.chefGuestsNumber,
            chefEventType: snapshot.chefEventType,
            cleanNoRooms: snapshot.cleanNoRooms,
            cleanAddRooms: snapshot.cleanAddRooms,
            customJob: snapshot.customJob,
            washNoClothes: snapshot.washNoClothes,
            washService: snapshot.washService,
            recycleType: snapshot.recycleType,
            paintNoWalls: snapshot.paintNoWalls,
            paintMaterials: snapshot.paintMaterials,
            fAssemblyNoUnits: snapshot.fAssemblyNoUnits,
            fMoveNoUnits: snapshot.fMoveNoUnits,
            fMoveDestAddress: snapshot.fMoveDestAddress
        };
        return job;
    }

    getQuotations(snapshot: any): Array<IQuotation> {
        let quotations: Array<IQuotation> = [];
        if (snapshot.val() == null)
            return quotations;

        let list = snapshot.val();

        Object.keys(snapshot.val()).map((key: any) => {
            let quotation: any = list[key];
            quotations.push({
                key: key,
                message: quotation.message,
                jobKey: quotation.jobKey,
                price: quotation.price,
                user: quotation.user,
                dateCreated: quotation.dateCreated
            });
        });

        return quotations;
    }

    getQuotation(snapshot: any, key: string): IQuotation {

        let quotation: IQuotation = {
            key: key,
            message: snapshot.message,
            dateCreated: snapshot.dateCreated,
            user: snapshot.user,
            price: snapshot.price,
            jobKey: snapshot.jobKey,
        };

        return quotation;
    }

    getBookedJobs(snapshot: any): Array<IJob> {
        let jobs: Array<IJob> = [];
        if (snapshot.val() == null)
            return jobs;

        let list = snapshot.val();

        Object.keys(snapshot.val()).map((key: any) => {
            let job: any = list[key];
            jobs.push({
                key: key,
                jobCategoryId: job.jobCategoryId,
                dateCreated: job.dateCreated,
                user: {
                    uid: job.user.uid,
                    username: job.user.username
                },
                quotations: job.quotations == null ? 0 : job.quotations,
                jobDate: job.jobDate,
                jobTime: job.jobTime,
                pincode: job.pincode,
                jobStatus: job.jobStatus,
                city: job.city,
                chefGuestsNumber: job.chefGuestsNumber,
                chefEventType: job.eventType,
                cleanNoRooms: job.cleanNoRooms,
                cleanAddRooms: job.cleanAddRooms,
                customJob: job.customJob,
                washNoClothes: job.washNoClothes,
                washService: job.washService,
                recycleType: job.recycleType,
                paintNoWalls: job.paintNoWalls,
                paintMaterials: job.paintMaterials,
                fAssemblyNoUnits: job.fAssemblyNoUnits,
                fMoveNoUnits: job.fMoveNoUnits,
                fMoveDestAddress: job.fMoveDestAddress,
                quotation: {
                    message: job.quotation.message,
                    jobKey: job.quotation.jobKey,
                    price: job.quotation.price,
                    user: job.quotation.user,
                    dateCreated: job.quotation.dateCreated
                }
            });
        });
        return jobs;
    }

    getChats(snapshot: any): Array<IMessage> {
        let chats: Array<IMessage> = [];
        if (snapshot.val() == null)
            return chats;

        let list = snapshot.val();

        Object.keys(snapshot.val()).map((key: any) => {
            let chat: any = list[key];
            chats.push({
                key: key,
                message: chat.message,
                quotationKey: chat.quotationKey,
                user: chat.user,
                dateCreated: chat.dateCreated
            });
        });

        return chats;
    }

    getChat(snapshot: any, key: string): IMessage {
        let chat: IMessage = {
            key: key,
            message: snapshot.message,
            quotationKey: snapshot.quotationkey,
            user: snapshot.user,
            dateCreated: snapshot.dateCreated
        }
        return chat;
    }
}