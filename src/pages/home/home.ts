import { Component, OnInit } from '@angular/core';

import { NavController, ModalController, Events, ToastController } from 'ionic-angular';
import { IJob } from '../../shared/interfaces';
import { DataService } from '../../shared/services/data.service';
import { AuthService } from '../../shared/services/auth.service';
import { ItemsService } from '../../shared/services/items.service';
import { MappingsService } from '../../shared/services/mappings.service';
import { JobComponent } from '../../shared/components/chefjob.component';
import { UserAvatarComponent } from '../../shared/components/user-avatar.component';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  public jobs: Array<IJob> = [];
  public category: string;
  public loading: boolean = true;

  constructor(public navCtrl: NavController,
    private modalCtrl: ModalController,
    private dataService: DataService,
    private authService: AuthService,
    private itemsService: ItemsService,
    private mappingsService: MappingsService,
    private toastCtrl: ToastController,
    private events: Events) {

  }

  ngOnInit() {
    var self = this;
    self.loadJobs(true);
    let uid = self.authService.getLoggedInUser().uid;
    self.events.subscribe('jobs:add', self.addNewJobs);
    this.dataService.getCategory(uid).then(function (snapshot) {
      self.category = snapshot.val();
      console.log('seller category' + self.category);
    self.dataService.getCategoryJobsRef().child(self.category).on('child_added', self.onJobAdded);
    });  
}

public onJobAdded = (childSnapshot, prevChildKey) => {
  var self = this;
  console.log('onJobAdded function Fired');
  self.events.publish('job:created');
}
public addNewJobs = () => {
  console.log('add new jobs event fired');
}

  loadJobs(fromStart: boolean) {
    var self = this;
    if (fromStart) 
    self.loading = true;
    self.jobs = [];
    let uid = self.authService.getLoggedInUser().uid;
    this.dataService.getCategory(uid).then(function (snapshot) {
      self.category = snapshot.val();
      console.log('seller category' + self.category);
      self.dataService.getCategoryJobsRef().child(self.category).orderByChild("jobStatus").equalTo('open').once('value', function (snapshot) {
        self.itemsService.reversedItems<IJob>(self.mappingsService.getJobs(snapshot)).forEach(function (job) {
          self.jobs.push(job);
          self.loading = false;
        });
        self.events.publish('jobs:viewed');
      });
    });
  }

  reloadJobs(refresher) {
    this.loadJobs(true);
    refresher.complete();
  }

}
