import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { LoginPage } from '../pages/login/login';
import { ChatsPage } from '../pages/chats/chats';
import { ChatPage } from '../pages/chat/chat';
import { ProfilePage } from '../pages/profile/profile';
import { ProfileImageUploadPage } from '../pages/profile-image-upload/profile-image-upload';
import { BookedJobsPage } from '../pages/booked-jobs/booked-jobs';
import { FeedbackPage } from '../pages/feedback/feedback';
import { SellerProfilePage } from '../pages/seller-profile/seller-profile';
import { CreateQuotationPage } from '../pages/create-quotation/create-quotation';
import { CustomJobsPage } from '../pages/custom-jobs/custom-jobs';
import { UserProfilePage } from '../pages/user-profile/user-profile';

import { JobComponent } from '../shared/components/job.component';
import { CustomJobComponent } from '../shared/components/customjob.component';
import { UserAvatarComponent } from '../shared/components/user-avatar.component';
import { BookedJobComponent } from '../shared/components/bookedjob.component';

import { UserData } from '../providers/user-data';
import { APP_PROVIDERS } from '../providers/app.providers';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    TutorialPage,
    SignUpPage,
    LoginPage,
    ChatsPage,
    ChatPage,
    ProfilePage,
    BookedJobsPage,
    SellerProfilePage,
    ProfileImageUploadPage,
    CreateQuotationPage,
    JobComponent,
    CustomJobComponent,
    UserAvatarComponent,
    BookedJobComponent,
    CustomJobsPage,
    FeedbackPage,
    UserProfilePage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    TutorialPage,
    SignUpPage,
    LoginPage,
    ChatsPage,
    ChatPage,
    ProfilePage,
    BookedJobsPage,
    SellerProfilePage,
    ProfileImageUploadPage,
    CreateQuotationPage,
    CustomJobsPage,
    FeedbackPage,
    UserProfilePage
  ],
  providers: [UserData, Storage, APP_PROVIDERS]
})
export class AppModule {}
