import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { IJob } from '../../shared/interfaces';
import { DataService } from '../../shared/services/data.service';
import { AuthService } from '../../shared/services/auth.service';
import { ItemsService } from '../../shared/services/items.service';
import { MappingsService } from '../../shared/services/mappings.service';
import { JobComponent } from '../../shared/components/job.component';

/*
  Generated class for the BookedJobs page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-booked-jobs',
  templateUrl: 'booked-jobs.html'
})
export class BookedJobsPage implements OnInit {
  public jobs: Array<IJob> = [];
  public newJobs: Array<IJob> = [];
  public loading: boolean = true;

  constructor(private navCtrl: NavController,
    private authService: AuthService,
    private dataService: DataService,
    private mappingsService: MappingsService,
    private itemsService: ItemsService,
    private events: Events) { }

  ionViewDidLoad() {
    console.log('Hello JobsPage Page');
  }
  ngOnInit() {
    var self = this;
    self.loadJobs(true);
    self.dataService.getBookedJobsRef().child(self.authService.getLoggedInUser().uid).on('child_added', self.onJobAdded);
  }

  public onJobAdded = (childSnapshot, prevChildKey) => {
  var self = this;
  console.log('onJobAdded function Fired');
  self.events.publish('bookedjob:created');
}

  loadJobs(fromStart: boolean) {
    var self = this;
    if (fromStart)
      self.loading = true;
    self.jobs = [];
    this.dataService.getBookedJobsRef().child(self.authService.getLoggedInUser().uid).orderByChild('jobCategoryId').once('value', function (snapshot) {
      self.itemsService.reversedItems<IJob>(self.mappingsService.getBookedJobs(snapshot)).forEach(function (job) {
        self.jobs.push(job);
        self.loading = false;
      });
    });
  }
  reloadJobs(refresher) {
    this.loadJobs(true);
    refresher.complete();
  }
}