import { Component, EventEmitter, OnInit, OnDestroy, Input, Output } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { IJob } from '../interfaces';
import { UserAvatarComponent } from '../../shared/components/user-avatar.component';
import { DataService } from '../services/data.service';
import { AuthService } from '../services/auth.service';
import { CreateQuotationPage } from '../../pages/create-quotation/create-quotation';

@Component({
    selector: 'all-job',
    templateUrl: 'job.component.html',
})

export class JobComponent implements OnInit, OnDestroy {
    @Input() job: IJob;
    public check: boolean

    constructor(private dataService: DataService,
        private authService: AuthService,
        private modalCtrl: ModalController) { }

    ngOnInit() {
        var self = this;
        console.log('pincode values at the job components are' + self.job.pincode);
        let uid = self.authService.getLoggedInUser().uid;
        self.dataService.checkUserQuotation(uid, self.job.key).then(function (snapshot) {
            self.check = snapshot.hasChild(self.job.key);
            console.log(self.job.key + ' exists in the table = ' + self.check);
        });
        self.dataService.getQuotationCheckRef().child(uid).child('quotations').on('child_added', self.onQuotationAdded);

    }

    ngOnDestroy() {

    }

    public onQuotationAdded = (childSnapshot, prevChildKey) => {
        console.log(childSnapshot.val());
        var self = this;
        // Attention: only number of comments is supposed to changed.
        // Otherwise you should run some checks..
        let uid = self.authService.getLoggedInUser().uid;
        self.dataService.checkUserQuotation(uid, self.job.key).then(function (snapshot) {
            self.check = snapshot.hasChild(self.job.key);
            console.log(self.job.key + ' exists in the table = ' + self.check);
        });
    }

    createQuotation(jobKey: string, jobUser: string) {
        console.log(jobKey + ' < job user id > ' + jobUser);
        let self = this;
        let modalPage = this.modalCtrl.create(CreateQuotationPage, { jobKey: jobKey, jobUser: jobUser });
        modalPage.present();
    }
}