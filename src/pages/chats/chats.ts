import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IQuotation } from '../../shared/interfaces';
import { DataService } from '../../shared/services/data.service';
import { AuthService } from '../../shared/services/auth.service';
import { ItemsService } from '../../shared/services/items.service';
import { MappingsService } from '../../shared/services/mappings.service';
import { JobComponent } from '../../shared/components/job.component';

import { ChatPage } from '../chat/chat';


/*
  Generated class for the Chats page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-chats',
  templateUrl: 'chats.html'
})
export class ChatsPage {
  public quotations: Array<IQuotation> = [];
  public newquotations: Array<IQuotation> = [];
  public loading: boolean = true;


  constructor(public navCtrl: NavController,
    public authService: AuthService,
    public dataService: DataService,
    public mappingsService: MappingsService,
    public itemsService: ItemsService) {}

  ionViewDidLoad() {
    console.log('Hello ChatsPage Page');
  }
  
  ngOnInit() {
    var self = this;
    self.loadChats();
  }

  loadChats() {
    var self = this;
    let uid = self.authService.getLoggedInUser().uid;
    console.log(uid);
    this.dataService.getQuotationsRef().orderByChild('user/uid').equalTo(uid).once('value', function (snapshot) {
      self.itemsService.reversedItems<IQuotation>(self.mappingsService.getQuotations(snapshot)).forEach(function (quotation) {
        self.quotations.push(quotation);
        console.log(quotation);
        self.loading = false;
      });
    });
  }

  gotoChat(quotationKey: string, jobKey: string) {
    this.navCtrl.push(ChatPage, { quotationKey: quotationKey, jobKey: jobKey });
  }

}
