import { Component, OnInit } from '@angular/core';
import { NavController, Events, Tabs } from 'ionic-angular';

import { HomePage } from '../home/home';
import { ChatsPage } from '../chats/chats';
import { BookedJobsPage } from '../booked-jobs/booked-jobs';
import { CustomJobsPage } from '../custom-jobs/custom-jobs';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;
  tab2Root: any = ChatsPage;
  tab3Root: any = BookedJobsPage;
  tab4Root: any = CustomJobsPage;

  public newJobs: string = '';
  public newBookedJobs: string = '';

  constructor(public events: Events) {

  }

  ngOnInit() {
    this.startListening();
  }

  startListening() {
    var self = this;
    self.events.subscribe('job:created', (jobData) => {
      if (self.newJobs === '') {
        self.newJobs = '1';
      } else {
        self.newJobs = (+self.newJobs + 1).toString();
      }
    });

    self.events.subscribe('bookedjob:created', (jobData) => {
      if (self.newBookedJobs === '') {
        self.newBookedJobs = '1';
      } else {
        self.newBookedJobs = (+self.newBookedJobs + 1).toString();
      }
    });

    self.events.subscribe('jobs:viewed', (jobData) => {
      self.newJobs = '';
    });
  }
}
