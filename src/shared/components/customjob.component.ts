import { Component, EventEmitter, OnInit, OnDestroy, Input, Output } from '@angular/core';
import { ModalController, ActionSheetController } from 'ionic-angular';
import { IJob } from '../interfaces';
import { UserAvatarComponent } from '../../shared/components/user-avatar.component';
import { DataService } from '../services/data.service';
import { CreateQuotationPage } from '../../pages/create-quotation/create-quotation';
import { SocialSharing } from 'ionic-native';

@Component({
    selector: 'custom-job',
    templateUrl: 'customjob.component.html',
})

export class CustomJobComponent {
    @Input() job: IJob;
    @Output() onViewQuotations = new EventEmitter<string>();

    constructor(private dataService: DataService,
        private modalCtrl: ModalController,
        private actionSheetCtrl: ActionSheetController) { }

    gotoQuotationPage(jobKey: string, jobUser: string) {
        console.log(jobKey + ' < job user id > ' + jobUser);
        let self = this;
        let modalPage = this.modalCtrl.create(CreateQuotationPage, { jobKey: jobKey, jobUser: jobUser });
        modalPage.present();
    }

    showShareActions() {
        var self = this;
        let actionSheet = self.actionSheetCtrl.create({
            title: 'Share Job',
            buttons: [
                {
                    text: 'WhatsApp',
                    icon: 'logo-whatsapp',
                    handler: () => {
                        self.whatsappShare();
                    }
                },
                {
                    text: 'Twitter',
                    icon: 'logo-twitter',
                    handler: () => {
                        self.twitterShare();
                    }
                },
                {
                    text: 'Facebook',
                    icon: 'logo-facebook',
                    handler: () => {
                        self.facebookShare();
                    }
                },
                {
                    text: 'Other',
                    icon: 'checkmark-circle',
                    handler: () => {
                        self.otherShare();
                    }
                },
                {
                    text: 'Cancel',
                    icon: 'close-circle',
                    role: 'cancel',
                    handler: () => { }
                }
            ]
        });

        actionSheet.present();
    }

    whatsappShare() {
        SocialSharing.shareViaWhatsApp("Found An Exciting job for you @solve", null /*Image*/, "http://solve.com/" /* url */)
            .then(() => {
                alert("Success");
            },
            () => {
                alert("failed")
            })
    }

    twitterShare() {
        SocialSharing.shareViaTwitter("Message via Twitter", null /*Image*/, "http://pointdeveloper.com")
            .then(() => {
                alert("Success");
            },
            () => {
                alert("failed")
            })
    }

    facebookShare() {
        SocialSharing.shareViaFacebook("Message via Twitter", null /*Image*/, "http://pointdeveloper.com")
            .then(() => {
                alert("Success");
            },
            () => {
                alert("failed")
            })
    }

    otherShare() {
        SocialSharing.share("Genral Share Sheet", null/*Subject*/, null/*File*/, "http://pointdeveloper.com")
            .then(() => {
                alert("Success");
            },
            () => {
                alert("failed")
            })

    }

}