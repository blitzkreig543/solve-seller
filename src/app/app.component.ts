import { Component, ViewChild, OnInit } from '@angular/core';
import { Platform, Nav, MenuController, ActionSheetController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { TutorialPage } from '../pages/tutorial/tutorial';
import { JobsPage } from '../pages/jobs/jobs';
import { BookedJobsPage } from '../pages/booked-jobs/booked-jobs';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ProfilePage } from '../pages/profile/profile';
import { FeedbackPage } from '../pages/feedback/feedback';
import { UserProfilePage } from '../pages/user-profile/user-profile';
import { SocialSharing } from 'ionic-native';
import { AuthService } from '../shared/services/auth.service';

export interface PageObj {
  title: string;
  component: any;
  icon: string;
}

@Component({
  templateUrl: `app.template.html`
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  appPages: PageObj[] = [
    { title: 'Home', component: HomePage, icon: 'home' },
    { title: 'Profile', component: UserProfilePage, icon: 'calendar' },
    { title: 'Give Feedback', component: FeedbackPage, icon: 'calendar' },
  ];

  rootPage: any;
  TutorialPage: TutorialPage;

  constructor(platform: Platform,
    public menu: MenuController,
    private actionSheetCtrl: ActionSheetController,
    public authService: AuthService) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  ngOnInit() {
    var self = this;

    this.authService.onAuthStateChanged(function (user) {
      if (user === null) {
        self.menu.close();
        self.nav.setRoot(TutorialPage);
      }
    });
  }

  hideSplashScreen() {
    if (Splashscreen) {
      setTimeout(() => {
        Splashscreen.hide();
      }, 100);
    }
  }

  openPage(page: PageObj) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }

  showShareActions() {
    var self = this;
    let actionSheet = self.actionSheetCtrl.create({
      title: 'Share Job',
      buttons: [
        {
          text: 'WhatsApp',
          icon: 'logo-whatsapp',
          handler: () => {
            self.whatsappShare();
          }
        },
        {
          text: 'Twitter',
          icon: 'logo-twitter',
          handler: () => {
            self.twitterShare();
          }
        },
        {
          text: 'Facebook',
          icon: 'logo-facebook',
          handler: () => {
            self.facebookShare();
          }
        },
        {
          text: 'Other',
          icon: 'checkmark-circle',
          handler: () => {
            self.otherShare();
          }
        },
        {
          text: 'Cancel',
          icon: 'close-circle',
          role: 'cancel',
          handler: () => { }
        }
      ]
    });

    actionSheet.present();
  }

  whatsappShare() {
    SocialSharing.shareViaWhatsApp("Found An Exciting job for you @solve", null /*Image*/, "http://solve.com/" /* url */)
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  twitterShare() {
    SocialSharing.shareViaTwitter("Message via Twitter", null /*Image*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  facebookShare() {
    SocialSharing.shareViaFacebook("Message via Twitter", null /*Image*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  otherShare() {
    SocialSharing.share("Genral Share Sheet", null/*Subject*/, null/*File*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })

  }

  showShareActions2() {
    var self = this;
    let actionSheet = self.actionSheetCtrl.create({
      title: 'Share Job',
      buttons: [
        {
          text: 'WhatsApp',
          icon: 'logo-whatsapp',
          handler: () => {
            self.whatsappShare2();
          }
        },
        {
          text: 'Twitter',
          icon: 'logo-twitter',
          handler: () => {
            self.twitterShare2();
          }
        },
        {
          text: 'Facebook',
          icon: 'logo-facebook',
          handler: () => {
            self.facebookShare2();
          }
        },
        {
          text: 'Other',
          icon: 'checkmark-circle',
          handler: () => {
            self.otherShare2();
          }
        },
        {
          text: 'Cancel',
          icon: 'close-circle',
          role: 'cancel',
          handler: () => { }
        }
      ]
    });

    actionSheet.present();
  }

  whatsappShare2() {
    SocialSharing.shareViaWhatsApp("Found An Exciting App for you @solve", null /*Image*/, "http://solve.com/" /* url */)
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  twitterShare2() {
    SocialSharing.shareViaTwitter("Message via Twitter", null /*Image*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  facebookShare2() {
    SocialSharing.shareViaFacebook("Message via Twitter", null /*Image*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })
  }

  otherShare2() {
    SocialSharing.share("Genral Share Sheet", null/*Subject*/, null/*File*/, "http://pointdeveloper.com")
      .then(() => {
        alert("Success");
      },
      () => {
        alert("failed")
      })

  }
}
