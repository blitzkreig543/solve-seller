import { Component, OnInit } from '@angular/core';
import { NavController, Modal, ViewController, LoadingController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { IQuotation, IUser } from '../../shared/interfaces';
import { AuthService } from '../../shared/services/auth.service';
import { DataService } from '../../shared/services/data.service';

/*
  Generated class for the CreateQuotation page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-create-quotation',
  templateUrl: 'create-quotation.html'
})
export class CreateQuotationPage implements OnInit {

  createQuotationForm: FormGroup;
  price: AbstractControl;
  message: AbstractControl;
  jobKey: string;
  jobUser: string;
  loaded: boolean = false;
  status: string = 'open';

  constructor(public navCtrl: NavController,
    private navParams: NavParams,
    private loadingCtrl: LoadingController,
    private viewCtrl: ViewController,
    private fb: FormBuilder,
    private authService: AuthService,
    private dataService: DataService,
    private toastCtrl: ToastController) { }

  ionViewDidLoad() {
    console.log('Hello CreateQuotationPage Page');
  }

  ngOnInit() {
    this.jobKey = this.navParams.get('jobKey');
    this.jobUser = this.navParams.get('jobUser');
    console.log('key' + this.jobKey);
    this.createQuotationForm = this.fb.group({
      'price': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
      'message': ['', Validators.compose([Validators.minLength(1)])]
    });

    this.price = this.createQuotationForm.controls['price'];
    this.message = this.createQuotationForm.controls['message'];
    this.loaded = true;
  }

  onSubmit(quotationForm: any): void {
    var self = this;
    if (this.createQuotationForm.valid) {

      let loader = this.loadingCtrl.create({
        content: 'Sharing Your Offer',
        dismissOnPageChange: true
      });

      loader.present();
      let uid = self.authService.getLoggedInUser().uid;
      let status = self.status;
      self.dataService.getUsername(uid).then(function (snapshot) {
        let username = snapshot.val();

        let quotationRef = self.dataService.getQuotationsRef().push();
        let quotationkey: string = quotationRef.key;
        console.log(quotationkey);
        let user: IUser = { uid: uid, username: username };

        let newQuotation: IQuotation = {
          key: quotationkey,
          jobKey: self.jobKey,
          message: quotationForm.message,
          price: quotationForm.price,
          user: user,
          dateCreated: new Date().toString(),
          status: status,
        };

        self.dataService.submitQuotation(self.jobKey, newQuotation, uid, self.jobUser)
          .then(function (snapshot) {
            loader.dismiss()
              .then(() => {
                self.viewCtrl.dismiss({
                  quotations: newQuotation,
                  user: user,
                  uid: uid
                });
              }).then(() => {
                let toast = self.toastCtrl.create({
                  message: 'Offer Shared Succesfully',
                  duration: 3000,
                  position: 'top'
                });
                toast.present();
              });
          }, function (error) {
            console.error(error);
            loader.dismiss();
          });
      });
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
