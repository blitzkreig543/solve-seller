import { Injectable } from '@angular/core';
import { IJob, IQuotation, IMessage } from '../interfaces';

declare var firebase: any;

@Injectable()
export class DataService {
    databaseRef: any = firebase.database();
    usersRef: any = firebase.database().ref('users');
    jobsRef: any = firebase.database().ref('jobsMaster');
    userJobsRef: any = firebase.database().ref('userjobs');
    storageRef: any = firebase.storage().ref();
    categoryJobsRef: any = firebase.database().ref('categoryJobs');
    quotationsRef: any = firebase.database().ref('quotations');
    userQuotationsRef: any = firebase.database().ref('userQuotations');
    bookedJobsRef: any = firebase.database().ref('bookedjobs');
    chatsRef: any = firebase.database().ref('chats');
    quotationCheckRef: any = firebase.database().ref('quotationCheck');

    connected: boolean = true;

    constructor() { }

    isFirebaseConnected() {
        return this.connected;
    }

    getUsersRef() {
        return this.usersRef;
    }

    getUserJobsRef() {
        return this.userJobsRef;
    }

    getStorageRef() {
        return this.storageRef;
    }

    getBookedJobsRef() {
        return this.bookedJobsRef;
    }

    getCategoryJobsRef() {
        return this.categoryJobsRef;
    }

    getQuotationsRef() {
        return this.quotationsRef;
    }

    getQuotationCheckRef() {
        return this.quotationCheckRef;
    }

    getChatRef() {
        return this.chatsRef;
    }

    getUsername(userUid: string) {
        return this.usersRef.child(userUid + '/username').once('value');
    }

    getChatJobRef(jobKey: string) {
        console.log('jobkey entering dataservice' + jobKey);
        return this.jobsRef.orderByKey().equalTo(jobKey);
    }

    getChatQuotationRef(quotationKey: string) {
        console.log('quotationkey recieved at dataservice' + quotationKey);
        return this.quotationsRef.orderByKey().equalTo(quotationKey);
    }

    getQuotation(quotationKey: string) {
        return this.quotationsRef.child(quotationKey).once('value');
    }

    getMessageRef(quotationKey: string) {
        return this.chatsRef.child(quotationKey);
    }

    submitChefJob(job: IJob, uid: string) {
        var newJobRef = this.jobsRef.push();
        var newJobKey = newJobRef.key;
        console.log(newJobKey);
        this.userJobsRef.child(uid).child(newJobKey).set(job);
        return newJobRef.set(job);
    }

    submitQuotation(jobKey: string, quotations: IQuotation, uid: string, jobUser: string) {
        this.quotationsRef.child(quotations.key).set(quotations);
        console.log('submitquotation function > dataservice' + uid, 'userId >' + jobUser);
        this.userQuotationsRef.child(jobUser).child(quotations.key).set(quotations);
        this.quotationCheckRef.child(uid + '/quotations/' + jobKey).set('true');
        return this.userJobsRef.child(jobUser).child(jobKey + '/quotations').once('value')
            .then((snapshot) => {
                let numberOfQuotations = snapshot == null ? 0 : snapshot.val();
                this.jobsRef.child(jobKey + '/quotations').set(numberOfQuotations + 1);
            });
    }

    submitChat(quotationKey: string, chats: IMessage) {
        this.chatsRef.child(quotationKey).child(chats.key).set(chats);
    }

    getCategory(userUid: string) {
        return this.usersRef.child(userUid + '/servicecategory').once('value');
    }

    checkUserQuotation(uid: string, jobKey: string) {
        return this.quotationCheckRef.child(uid).child('quotations').once('value');
    }

    checkUserImage(uid: string) {
        return this.usersRef.child(uid).once("value");
    }

    getUser(userUid: string) {
        return this.usersRef.child(userUid).once('value');
    }

    setUserImage(uid: string) {
        this.usersRef.child(uid).update({
            image: true
        });
    }
}